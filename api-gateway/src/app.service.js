import { Injectable, Dependencies, Inject } from "@nestjs/common";
// import { ClientProxy } from "@nestjs/microservices";
import { map } from "rxjs/operators";
//


@Injectable()
@Dependencies("SERVICE_A")
export class AppService {
  constructor(clientServiceA) {
    this.clientServiceA = clientServiceA;
  }
  pingServiceA() {
    const startTs = Date.now();
    const pattern = { cmd: "ping" };
    const payload = {};
    return this.clientServiceA
      .send(pattern, payload)
      .pipe(
        map((message) => ({ message, duration: Date.now() - startTs }))
      );
  }

  // postHello(data) {
  //   return this.clientServiceA.send('add',data);
  // }
  accumulate(data) {
    console.log('aaaa' + data);
    return this.clientServiceA.send('add', data);
  }
}

// let AppService = /** @class */ (() => {
//   var _a;
//   let AppService = class AppService {
//       constructor(clientServiceA) {
//           this.clientServiceA = clientServiceA;
//       }
//       pingServiceA() {
//           const startTs = Date.now();
//           const pattern = { cmd: "ping" };
//           const payload = {};
//           return this.clientServiceA
//               .send(pattern, payload)
//               .pipe(map((message) => ({ message, duration: Date.now() - startTs })));
//       }
//   };
//   AppService = __decorate([
//       Injectable(),
//       __param(0, Inject("SERVICE_A")),
//       __metadata("design:paramtypes", [typeof (_a = typeof ClientProxy !== "undefined" && ClientProxy) === "function" ? _a : Object])
//   ], AppService);
//   return AppService;
// })();
// export { AppService };