import { Controller, Dependencies, Get, Post, Body, Bind, Req } from '@nestjs/common';
// import { Controller, Logger, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
@Dependencies(AppService)
export class AppController {
  constructor(appService) {
    this.appService = appService;
  }


  @Get("/ping-a")
  pingServiceA() {
    return this.appService.pingServiceA();
  }

  @Post('hello')
  // async accumulate(@Body('data') data)  {
  @Bind(Req())
  async accumulate(req)  {
    let data = req.body.data;
    // this.logger.log('Adding ' + data.toString()); // Log something on every call
    // return this.appService.accumulate(data); // use math service to calc result & return
    console.log(data);
    return this.appService.accumulate(data);
  }
}
