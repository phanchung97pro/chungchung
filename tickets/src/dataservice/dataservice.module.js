import { Module, Dependencies } from '@nestjs/common';
import { DataserviceController } from './dataservice.controller';
import { DataserviceService } from './dataservice.service';
import mysqlx from '@mysql/xdevapi';
import { connect } from '../connect';

@Module({
  imports: [connect],
  controllers: [DataserviceController],
  providers: [DataserviceService],
})
export class DataserviceModule {}
