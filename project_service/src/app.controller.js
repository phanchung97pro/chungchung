
import { Controller, Dependencies } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { of } from "rxjs";
import { delay } from "rxjs/operators";
//
import { AppService } from './app.service';

@Controller()
@Dependencies(AppService)
export class AppController {
  constructor(appService) {
    this.appService = appService;
  }

  @MessagePattern({ cmd: "ping" })
  ping() {
    return of("pong").pipe(delay(1000));
  }

  @MessagePattern('add')
  // getHello() {
  //   return data;
  // }
  async accumulate(data)  {
    // this.logger.log('Adding ' + data.toString()); // Log something on every call
    return this.appService.accumulate(data); // use math service to calc result & return
  }

}
