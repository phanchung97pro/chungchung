import { Injectable } from '@nestjs/common';
import mysqlx from '@mysql/xdevapi';

@Injectable()
export class connect {
  constructor() {
    const dbCfg = {
      host: '127.0.0.1',
      port: 33060,
      user: 'root',
      password: '123456',
      schema: 'test',
    };
    let connect = mysqlx.getSession(dbCfg).then(session => {
      console.log('this is the session! ', session);
    });
    return connect;
  }
}
